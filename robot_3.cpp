#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <vector>

#include <queue>


using namespace std;

int m,n;



struct robo{
    int floor[101][101];   
    int state[4]; //P B C Move
    int mov;
    int x;
    int y;
    int h;
};
vector<robo> v;
bool operator<(const robo& a,const robo& b){
    return a.h>b.h;
}

bool operator==(const robo& a, const robo& b){
    if( a.x != b.x || a.y != b.y) return false;
    for(int i=1;i<=m;i++){
        for(int j=1;j<=n;j++){
            if(a.floor[i][j]!=b.floor[i][j]) return false;
        }
    }
   
    return true;
}
/*
    123
    405
    678
*/
bool notInQ(robo a){
   
    for(int i=0; i<v.size();i++){
        if(a==v[i]) return false;
    }
    return true;

}

bool isValid(int floor[101][101],int x, int y,int walk){
    
    if(walk == 1){ x = x-1; y = y+1;}
    else if(walk==2){x = x-1;}
    else if(walk==3){x=x-1;y=y+1;}
    else if(walk==4){y=y-1;}
    else if(walk==5){y=y+1;}
    else if(walk==6){x=x+1;y=y-1;}
    else if(walk==7){x=x+1;}
    else if(walk==8){x=x+1;y=y+1;}
    if(floor[x][y]<0) return false;
    return true;
}



void movXY(int walk, int &x, int &y){
    
    if(walk == 1){ x = x-1; y = y+1;}
    else if(walk==2){x = x-1;}
    else if(walk==3){x=x-1;y=y+1;}
    else if(walk==4){y=y-1;}
    else if(walk==5){y=y+1;}
    else if(walk==6){x=x+1;y=y-1;}
    else if(walk==7){x=x+1;}
    else if(walk==8){x=x+1;y=y+1;}
}

void clean(robo &robot){
    
    
    robot.state[2] = 3;

    while(robot.floor[robot.x][robot.y]>0){
            robot.mov = robot.mov+1;
            robot.floor[robot.x][robot.y] = abs(robot.floor[robot.x][robot.y]-robot.state[2]);
            robot.state[2]=1;
    }
  

}
int calH(robo t){
    int h=0;
    for(int i=1;i<=m;i++){
        for(int j=1;j<=n;j++){
            if(t.floor[i][j]>0)
                h+=1;
        }
    }
    return h;
}

int main(){
    
    robo robot;
    cin>>m>>n;
    //input data
    for(int i=0;i<m+2;i++){
        for(int j=0;j<n+2;j++){
            if(i==0||i==m+1||j==0||j==n+1)
                robot.floor[i][j] = -1;
            else{
                int tmp;
                cin>>tmp;
                if(tmp == 0) robot.floor[i][j] = -1;
                else robot.floor[i][j] = tmp;
            }
        }
    }

    robot.x = 1;
    robot.y = 1;
    robot.mov = 0;
  	robot.state[0] =2;
  	robot.state[1] =1;
  	robot.state[2]=0;
  	robot.state[3]=0;
    robot.h = calH(robot);
    priority_queue<robo> q;
    q.push(robot);
    while(q.size() != 0){
        //cout<<q.size()<<" "<<v.size()<<endl;
        robo qHead;

        qHead = q.top(); q.pop();
        for(int i=1;i<=m;i++){
                for(int j=1;j<=n;j++){
                    cout<<qHead.floor[i][j]<<" ";
                }
                cout<<endl;
            }
        cout<<"****"<<endl;   

       if(qHead.mov>2000 || qHead.h==0){
                cout<<"-----------------"<<qHead.mov<<"--------------"<<endl;
            for(int i=1;i<=m;i++){
                for(int j=1;j<=n;j++){
                    cout<<qHead.floor[i][j]<<" ";
                }
                cout<<endl;
            }
            if (qHead.h ==0) break;
        }
      
      if(notInQ(qHead)){
      	for(int i=1; i<9;i++){
      		robo t = qHead;
      		if(isValid(t.floor,t.x,t.y,i)){
      			movXY(i,t.x,t.y);
      			clean(t);
      			t.h = calH(t);
      			t.mov = t.mov+1;
      			
      			q.push(t);
			  }
		  }
    	v.push_back(qHead);
     
    }
}}

